
var comNpay = {};

// To replace with your identifiers
var serialNumber = "HOM-875-660";

// To replace with the url of your website
var origin = "your.website.com";

// Generic settings
var homologation_url = "https://secure-homologation.comnpay.com:60000";
var production_url = "https://secure.comnpay.com:60000";
var api = "/rest/token/create";
var url = "";

var cardType = "";



// Mask initialization for payment
comNpay.initMask = function(){
    // Card mask
    $('#card').mask('0000 0000 0000 0000 000');
    // Date mask
    $('#date').mask('00/00');
    // Cvv mask
    $('#cvv').mask('0000');
};

// Initialization of checks for input format checking
comNpay.initCheck = function(){

    // Card check
    $("#card").focusout(function() {
        comNpay.checkCard();
    });

    // Date check
    $("#date").focusout(function() {
        comNpay.checkDate();
    });

    // Cvv check
    $("#cvv").focusout(function() {
        comNpay.checkCvv();
    });
};


// Card check
comNpay.checkCard = function(){
    var card = $("#card");
    if(!isValidCard(card.val())){
        card.addClass('errorForm');
        $("#tokenRef").val('');
        return false;
    }else{
        card.removeClass('errorForm');
        return true;
    }
};

// Date check
comNpay.checkDate = function(){
    var re = new RegExp("0[1-9]|10|11|12/[0-9][0-9]");
    var date = $("#date");
    if(!re.test(date.val())){
        date.addClass('errorForm');
        $("#tokenRef").val('');
        return false;
    }else{
        date.removeClass('errorForm');
        return true;
    }
};

// Cvv check
comNpay.checkCvv = function(){
    var cvv = $("#cvv");
    if(cvv.val().length > 4 || cvv.val().length < 3){
        cvv.addClass('errorForm');
        $("#tokenRef").val('');
        return false;
    }else{
        cvv.removeClass('errorForm');
        return true;
    }
};


// Type check
comNpay.checkType = function(){
    var cvv = $("#cvv");
    if(cardType === ''){
        $(".imgToSelect").each(function () {
            $(this).addClass("errorFormCard");
        });
        $("#tokenRef").val('');
        return false;
    }else{
        cvv.removeClass('errorForm');
        return true;
    }
};


// Initializing ComNpay settings
comNpay.init = function(plateform, canUseAmex){
    if(plateform === "HOMOLOGATION"){
        url = homologation_url + api;
    }else{
        url = production_url + api;
    }

    if (!canUseAmex){
        $("#canUseAmex").remove();
    }




};

// Tokenization
comNpay.tokenize = function(cardNumber, cardExpirationDate, cvv){
    // If all fields are filled correctly:
    if(comNpay.checkType() && comNpay.checkCard() && comNpay.checkDate() && comNpay.checkCvv()){
        $.get("/index.php?fc=module&module=EasyPay&controller=payroute&serial-number",function(json){
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    serialNumber: json.serial,
                    cardNumber: cardNumber.replace(/\s/g,''),
                    cardExpirationDate: cardExpirationDate,
                    cvv: cvv,
                    cardType: cardType,
                    origin: origin
                },
                dataType: "json"
            }).done(function (msg) {
                if(msg["ok"]){
                    $("#token").html(msg['token']['tokenRef']);
                    $("#tokenExample").html(msg['token']['tokenRef']);
                    $("#tokenRef").val(msg['token']['tokenRef']);
                    $("#next").show();
                    /*$("#card").val("");
                    $("#date").val("");
                    $("#cvv").val("");*/
                    $("#titleToken").show();
                }else{
                    $("#tokenRef").val('');
                    $("#token").html("ERREUR : " + msg['message']);
                    $("#next").hide();
                    $("#nextStep").hide();
                    $("#titleToken").hide();
                }
            });
        })
        return true;
    }else{
        return false;
    }
};


// Verification of the format of the card
function isValidCard(identifier) {
    identifier = identifier.replace(/\s/g,'');
    var sum     = 0,
        alt     = false,
        i       = identifier.length-1,
        num;
    if (identifier.length < 13 || identifier.length > 19){
        return false;
    }
    while (i >= 0){
        num = parseInt(identifier.charAt(i), 10);
        if (isNaN(num)){
            return false;
        }
        if (alt) {
            num *= 2;
            if (num > 9){
                num = (num % 10) + 1;
            }
        }
        alt = !alt;
        sum += num;
        i--;
    }
    return (sum % 10 == 0);
}



$(document).ready(function() {
    // Select good card
    $(".imgToSelect").click(function () {
        $(".imgToSelect").each(function () {
            $(this).removeClass("selectedCard");
            $(this).removeClass("errorFormCard");
        });
        $(this).addClass("selectedCard");
        cardType = $(this).data("id");
    });
});

