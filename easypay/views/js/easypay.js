/**
 * Handles the FE payment flow
 */
function EasyPay(){
  "use strict"
  var self = this;

  self.container = $(".easy-pay-container");
  self.initComNPay();
  self.tokenizeCard();
  self.checkCardDetails();
  self.payHandler();
  $(".payment-option > label > span").each(function(){
    if($(this).text()=="EasyPay") $(this).text("ComNpay");
  });
  $("[data-module-name='EasyPay']").parent().find(".custom-control-description").text("ComNpay");
}

/**
 * Inits the comnpay SDK
 */
EasyPay.prototype.initComNPay = function() {
  "use strict"
  var self = this;
  
  comNpay.initMask();

  // Initialization of checks for input format checking
  comNpay.initCheck();

  // Initializing ComNpay settings
  // Replace by 'PRODUCTION' for real tokenization for payment
  // init with HOMOLOGATION or PRODUCTION for real payments
  // init with a second param to true to be able to use American Express
  comNpay.init(self.container.data("gateway"));
}

/**
 * Tokenize card if all the card info are filled
 */
EasyPay.prototype.tokenizeCard = function() {
  "use strict"
  var self = this;

  

  self.container.find("input").keyup(self.makeTokenizeCard.bind(self));
  self.container.find(".imgToSelect").click(self.makeTokenizeCard.bind(self));
}

/**
 * Tokenize the card details
 */
EasyPay.prototype.makeTokenizeCard = function(){
  "use strict"
  var self = this;

  if(
    comNpay.tokenize(
      self.container.find("#card").val(), 
      self.container.find("#date").val(), 
      self.container.find("#cvv").val()
    )
  ){
    var cardToken = self.container.find("#tokenRef").val();
    self.container.find("#payButton").prop("disabled",false);
  }
}

/**
 * Checks if the card is filled and token is generated
 */
EasyPay.prototype.checkCardDetails = function() {
  "use strict"
  var self = this;

  setInterval(function() {
    $("#conditions-to-approve, #payment-confirmation").toggle(false==self.container.is(":visible"));
    if(self.container.is(":visible")){
      self.container.find("#payButton").prop("disabled",self.container.find("#tokenRef").val().length==0);
      self.setStatusPSPaymentButton(null===self.readCookie('transaction_ok'));
    }
  },300)
}

/**
 * Pay button handler
 */
EasyPay.prototype.payHandler = function() {
  "use strict"
  var self = this;

  self.container.find("#payButton").click(function() {
    self.container.find(".text-error").hide();
    self.container.find(".description-error").html("");
    $.LoadingOverlay('show');
    $.get("/index.php?fc=module&module=EasyPay&controller=payroute",{
      token: self.container.find("#tokenRef").val()
    },function(json){
      if(json.responseCode!="000"){
        $.LoadingOverlay('hide');
        self.setStatusPSPaymentButton(true);
        self.container.find(".description-error").html(json.message).show();
      }else if(json.actionCode=="AUTH_3DS_REQUISE"){
        var threeds_form = self.container.find("#3ds");
        threeds_form.attr("action",json.transaction.verifyEnrollment3dsActionUrl);
        if(json.transaction.verifyEnrollment3dsV1){
          threeds_form.find("[name='MD']").val(json.transaction.verifyEnrollment3dsMd);
          threeds_form.find("[name='PaReq']").val(json.transaction.verifyEnrollment3dsPareq);
        }else{
          threeds_form.find("[name='creq']").val(json.transaction.verifyEnrollment3dsCreq);
        }
        
        threeds_form.submit();
        self.startCheckTransaction3dsResult();
      }else{
        //$.LoadingOverlay('hide');
        self.container.find("#payForm").hide();
        self.container.find(".text-success").show();
        self.setStatusPSPaymentButton(false);
        $("#payment-confirmation button").trigger("click");
      }
    });
  });
}

/**
 * Enables or not the PS order button
 * 
 * @param {*} isDisabled 
 */
EasyPay.prototype.setStatusPSPaymentButton = function(isDisabled){
  "use strict"
  var self = this;

  $("#payment-confirmation button").prop("disabled",isDisabled);
}

/**
 * Checks if the transaction_ok/transaction_error cookie is available
 */
EasyPay.prototype.startCheckTransaction3dsResult = function(){
  "use strict"
  var self = this;

  self.checkTransactionID = setInterval(function(){
    if(self.readCookie("transaction_ok")){
      clearInterval(self.checkTransactionID);
      self.container.find("#payForm, .text-error").hide();
      self.container.find(".text-success").show();
      $("#payment-confirmation button").prop("disabled",false).trigger("click");
      //$.LoadingOverlay('hide');
    }else if(self.readCookie("transaction_error")){
      clearInterval(self.checkTransactionID);
      $.LoadingOverlay('hide');
      self.container.find(".text-success").hide();
      self.container.find(".text-error").show();
    }
  },3000);
}

/**
 * Read cookie by name
 * 
 * @param {*} name 
 */
EasyPay.prototype.readCookie = function(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}