<div class="align-center easy-pay-container" data-gateway="{$gateway}">
    <div class="comnpay column medium-6 center-text-for-small-only">
        <form id="3ds" method="post" target="_blank">
            <input type="hidden" name="TermUrl" value="https://{$smarty.server.HTTP_HOST}/index.php?fc=module&module=EasyPay&controller=payroute"/>
            <input type="hidden" name="MD" value=""/>
            <input type="hidden" name="PaReq" value=""/>
            <input type="hidden" name="creq" value=""/>
        </form>
        <form id="payForm" method="POST" action="" style="{if $status_cart_payment=='paid'}display: none;{/if}">
            <input type="hidden" id="tokenRef" name="tokenRef">
            <div class="comnpay-detail">
                <h3>{l s='Seleziona la tipologia di carta' mod='EasyPay'}</h3><br/>
                <div class="container row">
                    <div class="col-xs-6 col-md-2 text-center">
                        <img src="{$module_dir|escape:'html':'UTF-8'}views/img/logo-cb.jpg" class="imgToSelect cb" data-id="CB" width="70px">
                    </div>
                    <div class="col-xs-6 col-md-2 text-center">
                        <img src="{$module_dir|escape:'html':'UTF-8'}views/img/logo-visa.png" class="imgToSelect visa" data-id="VISA" width="70px">
                    </div>
                    <div class="col-xs-6 col-md-2 mt-xs-2 text-center">
                        <img src="{$module_dir|escape:'html':'UTF-8'}views/img/logo-mc.png" class="imgToSelect mc" data-id="MC" width="70px">
                    </div>
                    <div class="col-xs-6 col-md-2 mt-xs-2 text-center" id="canUseAmex">
                        <img src="{$module_dir|escape:'html':'UTF-8'}views/img/logo-amex.jpeg" class="imgToSelect amex" data-id="AMEX" width="70px">
                    </div>
                    <div class="cell auto text-center"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-12">
                        <span class="input-group-label"><i class="fa fa-credit-card"></i></span>
                        <input class="form-control card-input" type="text" placeholder="{l s='Numero della carta' mod='EasyPay'}" id="card">
                    </div>
                    <div class="col-12 mt-2">
                        <div class="row">
                            <div class="col-6">
                                <input class="form-control" type="text" placeholder="{l s='Data di scadenza (mm/aa)' mod='EasyPay'}" id="date">
                            </div>
                            <div class="col-6">
                                <input class="form-control" type="text" placeholder="{l s='CVV' mod='EasyPay'}" id="cvv">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <button class="pull-right btn btn-primary text-white mt-1" id="payButton" type="button" disabled>{l s='Effettua il pagamento' mod='EasyPay'}</button>
                        <p class="description-error text-error"></p>
                    </div>
                </div>
            </div>
        </form>
        <h3 class="text-success" style="{if $status_cart_payment!='paid'}display: none;{/if}">{l s='Complimenti! Pagamento andato a buon fine, puoi adesso procedere all\'invio dell\'ordine'|escape:'html' mod='EasyPay'}</h3>
        <h3 class="text-error" style="{if $status_cart_payment!='error'}display: none;{/if}">{l s='Il pagamento non è andato a buon fine, si prega di ritentare'|escape:'html' mod='EasyPay'}</h3>
    </div>
</div>