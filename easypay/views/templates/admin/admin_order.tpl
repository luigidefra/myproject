<div class="panel easypay-container">
    <div class="panel-heading"><i class="icon-user"></i> {l s='ComNpay' mod='EasyPay'}</div>
    <div class="row">
        <div class="form-wrapper">
          {if empty($refund_date)}
            <button type="button" class="btn btn-primary" data-order="{$order_id}" data-cart="{$cart_id}" data-transaction="{$transaction_ref}">{l s='Rimborsa ordine' mod='EasyPay'}</button>
          {else}
            {l s='Rimborso effettuato in data %1$s' sprintf=$refund_date}
          {/if}
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
<script>
$(document).ready(function(){
  $.LoadingOverlaySetup({
    imageColor: '#25aae1'
  });

  $(".easypay-container button").click(function(){
    if(!confirm("Sei sicuro di voler effettuare il rimborso? Una volta confermato non sarà più possibile annullare l'operazione")) return;
    if($(this).data("transaction").length==0) return alert("Impossibile rimborsare l'ordine");
    $.LoadingOverlay('show');
    
    $.get("/index.php?fc=module&module=ComNpay&controller=payroute",{
      transaction_ref: $(this).data("transaction"),
      cart_id: $(this).data("cart"),
      order_id: $(this).data("order")
    },function(json){
      $.LoadingOverlay('hide');
      if(json.responseCode=="200"){
        alert("Rimborso avvenuto con successo");
        window.location.reload();
      }else{
        alert("Qualcosa è andato storto durante il rimborso, si prega di riprovare. Se il problema persiste allora contattare l'amministratore di sistema per effettuare verifiche approfondite");
      }
    })
  });
});
</script>