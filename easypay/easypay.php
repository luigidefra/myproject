<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class easypay extends PaymentModule
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'EasyPay';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'HexLab Software';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('ComNpay');
        $this->description = $this->l('Consente il pagamento tramite carta impiegando il gateway ComNPay https://comnpay.com/');

        $this->confirmUninstall = $this->l('Sei sicuro di volere disinstallare il modulo?');

        $this->limited_countries = array('IT');

        $this->limited_currencies = array('EUR');

        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (extension_loaded('curl') == false)
        {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        if (in_array($iso_code, $this->limited_countries) == false)
        {
            $this->_errors[] = $this->l('This module is not available in your country');
            return false;
        }

        Configuration::updateValue('EASYPAY_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('paymentOptions') &&
            $this->registerHook('displayPaymentReturn') && 
            $this->registerHook('displayAdminOrder');
    }

    public function uninstall()
    {
        Configuration::deleteByName('EASYPAY_LIVE_MODE');
        Configuration::deleteByName('EASYPAY_KEY');
        Configuration::deleteByName('EASYPAY_SANDBOX');
        Configuration::deleteByName('EASYPAY_SERIAL_NUMBER');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitEasyPayModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitEasyPayModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Abilita il metodo di pagamento'),
                        'name' => 'EASYPAY_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Rendi disponibile il metodo di pagamento in FrontEnd'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Abilita la sandbox'),
                        'name' => 'EASYPAY_SANDBOX',
                        'is_bool' => true,
                        'desc' => $this->l('Abilita o meno il gateway di test per effettuare i test sul flusso di pagamento'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-code"></i>',
                        'desc' => $this->l('Inserisci il numero seriale, se abiliti la sandbox verrà impostato automaticamente il seguente numero seriale: DEMO'),
                        'name' => 'EASYPAY_SERIAL_NUMBER',
                        'label' => $this->l('Numero Seriale'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-key"></i>',
                        'desc' => $this->l('Inserisci la chiave, se abiliti la sandbox verrà impostata automaticamente la seguente chiave: DEMO'),
                        'name' => 'EASYPAY_KEY',
                        'label' => $this->l('Chiave'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Salva'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'EASYPAY_LIVE_MODE' => Configuration::get('EASYPAY_LIVE_MODE', true),
            'EASYPAY_SANDBOX' => Configuration::get('EASYPAY_SANDBOX', false),
            'EASYPAY_SERIAL_NUMBER' => Configuration::get('EASYPAY_SERIAL_NUMBER', ''),
            'EASYPAY_KEY' => Configuration::get('EASYPAY_KEY', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->registerJavascript(
            'comnpay-sdk',
            $this->_path.'/views/js/comnpay-sdk.js',
            array(
                'priority' => 8
            )
        );
        $this->context->controller->registerJavascript(
            'foundation.min',
            $this->_path.'/views/js/foundation.min.js',
            array(
                'priority' => 9
            )
        );
        $this->context->controller->registerJavascript(
            'jquery.mask', 
            'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js', 
            array(
                'media' => 'all', 
                'priority' => 10, 
                'inline' => true, 
                'server' => 'remote'
            )
        );
        $this->context->controller->registerJavascript(
            'easypay',
            $this->_path.'/views/js/easypay.js',
            array(
                'priority' => 24
            )
        );
        $this->context->controller->registerJavascript(
            'loadingoverlay', 
            'https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js', 
            array(
                'media' => 'all', 
                'priority' => 12, 
                'inline' => true, 
                'server' => 'remote'
            )
        );
        $this->context->controller->registerJavascript(
            'front',
            $this->_path.'/views/js/front.js',
            array(
                'priority' => 13
            )
        );
        $this->context->controller->registerStylesheet(
            'fontawesome',
            'https://use.fontawesome.com/releases/v5.0.10/css/all.css',
            array(
                'media' => 'all',
                'priority' => 9,
                'inline' => true,
                'server' => 'remote'
            )
        );
        $this->context->controller->registerStylesheet(
            'comnpay',
            $this->_path.'/views/css/comnpay.css',
            array(
                'priority' => 10
            )
        );
        $this->context->controller->registerStylesheet(
            'front',
            $this->_path.'/views/css/front.css',
            array(
                'priority' => 12
            )
        );
    }

    /**
     * Return payment options available for PS 1.7+
     *
     * @param array Hook parameters
     *
     * @return array|null
     */
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart']) || !Configuration::get('EASYPAY_LIVE_MODE',false)) {
            return;
        }

        /**
         * Prepare the payment form when the payment method is selected
         */
        $statusCartPayment = Db::getInstance()->executeS("SELECT status FROM "._DB_PREFIX_."EasyPay WHERE id_cart=".$this->context->cart->id);
        
        if(!empty($statusCartPayment) && $statusCartPayment[0]['status']=='paid') setcookie("transaction_ok",1,strtotime("+30 minute"),"/");
        
        $this->context->smarty->assign([
            'module_dir' => $this->_path,
            'gateway' => Configuration::get('EASYPAY_SANDBOX',false) ? 'HOMOLOGATION' : 'PRODUCTION',
            'status_cart_payment' => empty($statusCartPayment) ? '' : $statusCartPayment[0]['status']
        ]);
        $paymentForm = $this->context->smarty->fetch($this->local_path.'views/templates/hook/payment_option_form.tpl');
        
        $option = new \PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $option->setModuleName($this->l($this->name))
            ->setCallToActionText($this->l($this->name))
            ->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true))
            ->setAdditionalInformation($paymentForm);

        return [
            $option
        ];
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);
        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function hookDisplayPaymentReturn()
    {
        /* Place your code here. */
    }

    public function hookDisplayAdminOrder($params){
        $transaction = Db::getInstance()->getRow("SELECT transaction_ref,status,DATE_FORMAT(created_at,'%d/%m/%Y %H:%i:%s') created_at FROM "._DB_PREFIX_."EasyPay WHERE id_cart=".$this->context->cart->id);

        $this->context->smarty->assign([
            "transaction_ref" => $transaction["transaction_ref"],
            "refund_date" => $transaction["status"]=="refund" ? $transaction["created_at"] : "",
            "cart_id" => $this->context->cart->id,
            "order_id" => $params['id_order']
        ]);
        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/admin_order.tpl');
    }
}
