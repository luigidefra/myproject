<?php

/**
 * This module will make requests to ComNPay API in order to make a transaction
 */
class EasyPayPayRouteModuleFrontController extends ModuleFrontController{
  
  const SANDBOX_ENDPOINT = "https://secure-homologation.comnpay.com:60000/rest/payment/";
  const PRODUCTION_ENDPOINT = "https://secure.comnpay.com:60000/rest/payment/";
  const DEMO = [
    "HOM-875-660",
    "t20jP1n2EnJbiKUkE46I"
  ];

  public function postProcess()
  {    
    $url = Configuration::get("EASYPAY_SANDBOX",false) ? self::SANDBOX_ENDPOINT : self::PRODUCTION_ENDPOINT;
    $serialNumber = Configuration::get("EASYPAY_SERIAL_NUMBER",'');
    $key = Configuration::get("EASYPAY_KEY",'');
    $serialNumber = Configuration::get("EASYPAY_SANDBOX",false) ? (empty($serialNumber) ? self::DEMO[0] : $serialNumber) : $serialNumber;
    $key = Configuration::get("EASYPAY_SANDBOX",false) ? (empty($key) ? self::DEMO[1] : $key) : $key;

    /**
     * Validate 3ds flow
     */
    if(isset($_POST['MD']) || isset($_POST['cres'])){
      if(isset($_COOKIE["transaction_ref"])){
        $params = [
          "transactionRef" => $_COOKIE["transaction_ref"],
        ];
        if(array_key_exists("MD",$_POST)){
          $params = array_merge($params,[
            "pares" => $_POST["PaRes"],
            "md" => $_POST["MD"]
          ]);
        }else{
          $params = array_merge($params,[
            "cres" => $_POST["cres"]
          ]);
        }
        $r = $this->makeComNPayRequest("{$url}end3dsDebit",$serialNumber,$key,$params);
        $json = json_decode($r);

        Db::getInstance()->execute("UPDATE "._DB_PREFIX_."EasyPay SET status='".($json->responseCode=="200" ? 'paid' : 'error')."' WHERE transaction_ref='".$_COOKIE['transaction_ref']."'");
        if($json->responseCode=="200"){
          setcookie("transaction_ref","",time()-3600,"/");
          setcookie("transaction_ok",1,strtotime("+30 minute"),"/");
        }else{
          setcookie("transaction_error",1,strtotime("+30 minute"),"/");
        }
      }
      exit(
        "<script>window.close();</script>"
      );
    }

    $cart = $this->context->cart;
    header("Content-Type: application/json");

    /**
     * Returns the serial number
     */
    if(isset($_GET['serial-number'])){
      exit(
        json_encode(
          [
            'serial' => $serialNumber
          ]
        )
      );
    }

    /**
     * Make the refund request
     */
    if(isset($_GET["transaction_ref"])){
      $cart = new Cart((int) $_GET["cart_id"]);
      $response = $this->makeComNPayRequest("{$url}refund",$serialNumber,$key,[
        'transactionRef' => $_GET["transaction_ref"],
        'amount' => str_replace(".","",$cart->getOrderTotal(true, Cart::BOTH))
      ]);
      $json = json_decode($response);
      if($json->responseCode=="200"){
        Db::getInstance()->execute("UPDATE "._DB_PREFIX_."EasyPay SET status='refund' WHERE transaction_ref='{$_GET['transaction_ref']}'");
        (new Order($_GET["order_id"]))->setCurrentState((int) Configuration::get('PS_OS_REFUND'));
      }
      exit(
        $response
      );
    }

    $cart = $this->context->cart;
    $customer = $this->context->customer;
    $addressInvoice = (new Address($cart->id_address_invoice));
    $addressDelivery = (new Address($cart->id_address_delivery));
    /**
     * Make the tokenDebit request
     */
    $response = $this->makeComNPayRequest("{$url}tokenDebit",$serialNumber,$key,[
      "tokenRef" => $_GET['token'],
      "amount" => str_replace(".","",$cart->getOrderTotal(true, Cart::BOTH)),
      "redirection3DSV2Url" => "https://".$_SERVER['HTTP_HOST']."/index.php?fc=module&module=EasyPay&controller=payroute",
      'customer' => json_encode(array(
        "firstName"=>$customer->firstname,
        "lastName"=>$customer->lastname,
        "email"=>$customer->email,
        "phone"=>"",
        "personalPhone"=>"+3241565858",
        "professionalPhone"=>"+33707070707",
        "road"=> $addressInvoice->address1,
        "road2"=>$addressInvoice->address2,
        "road3"=>"",
        "zipCode"=>$addressInvoice->postcode,
        "city"=>$addressInvoice->city,
        "country"=>$addressInvoice->country,
        "shipRoad"=>$addressDelivery->address1,
        "shipRoad2"=>$addressDelivery->address2,
        "shipRoad3"=>"",
        "shipZipCode"=>$addressDelivery->postcode,
        "shipCity"=> $addressDelivery->city,
        "shipCountry"=> $addressDelivery->country,
        "meetingDate"=>"",
        "customerRef"=>""
      )),
      "browser" => json_encode(
        array(
          "browserAcceptHeader" => implode("\n",get_headers("http://".$_SERVER['HTTP_HOST'])),
          "browserIP" => Tools::getRemoteAddr(),
          "browserJavaEnabled" => true,
          "browserLanguage" => $this->context->language->iso_code,
          "browserColorDepth" => 24,
          "browserScreenHeight" => 1080,
          "browserScreenWidth" => 1920,
          "browserTZ"=>"-120",
          "browserUserAgent"=> Tools::getUserBrowser(),
          "challengeWindowSize"=>"05",
          "browserJavascriptEnabled"=>true
        )
      ),
    ]);
    $json = json_decode($response);
    if($json->responseCode!="000"){
    	exit($response);
    }
    $is3DS = $json->actionCode=="AUTH_3DS_REQUISE";
    
    /**
     * Set the correct status for the current transaction
     */
    $transactionRef = $json->transaction->transactionRef;
    Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."EasyPay WHERE id_cart=".$cart->id." AND status='pending'");
    if(empty(Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."EasyPay WHERE id_cart=".$cart->id))){
      Db::getInstance()->execute("INSERT INTO "._DB_PREFIX_."EasyPay (id_cart,transaction_ref,status) VALUES (".$cart->id.",'{$transactionRef}','".(!$is3DS ? 'paid' : 'pending')."')");
    }else{
      Db::getInstance()->execute("UPDATE "._DB_PREFIX_."EasyPay SET transaction_ref='{$transactionRef}'".(!$is3DS ? ", status='paid'" : "")." WHERE id_cart=".$cart->id);
    }
    if($is3DS) setcookie("transaction_ref",$transactionRef,strtotime("+1 year"),"/");

    exit(
      $response
    );
  }

  /**
   * Make a call to comnpay API
   */
  private function makeComNPayRequest($url,$serialNumber,$key,$params = []){
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "serialNumber={$serialNumber}&key={$key}&".http_build_query($params),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/x-www-form-urlencoded"
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);

    return $response;
  }
}